import 'package:flutter/gestures.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/rendering.dart';

import 'theme.dart';
import 'desktop.dart';

const _kHeaderHeight = 38.0;
const _kMinColumnWidth = 38.0;
const _kHandlerWidth = 8.0;
const _kDefaultItemExtent = 40.0;

typedef TableHeaderBuilder = Widget Function(
  BuildContext context,
  int col,
  BoxConstraints colConstraints,
);

typedef TableRowBuilder = Widget Function(
  BuildContext context,
  int row,
  int col,
  BoxConstraints colConstraints,
);

//typedef RowMouseEvent = void Function(int row, MouseEvent event);

typedef RowPressedCallback = void Function(int index);

class ListTable extends StatefulWidget {
  const ListTable({
    this.tableBorder,
    @required this.colCount,
    @required this.itemCount,
    @required this.tableHeaderBuilder,
    @required this.tableRowBuilder,
    this.headerColumnBorder,
    this.colFraction,
    this.controller,
    this.itemExtent = _kHeaderHeight,
    this.showHiddenColumnsIndicator = true,
    this.onPressed,
    Key key,
  })  : assert(colCount > 0),
        assert(itemExtent != null && itemExtent >= 0.0),
        assert(itemCount != null),
        assert(tableHeaderBuilder != null),
        assert(tableRowBuilder != null),
        super(key: key);

  final BorderSide headerColumnBorder;

  final int colCount;

  final int itemCount;

  final Map<int, double> colFraction;

  final TableRowBuilder tableRowBuilder;

  final TableHeaderBuilder tableHeaderBuilder;

  final TableBorder tableBorder;

  final bool showHiddenColumnsIndicator;

  final ScrollController controller;

  final double itemExtent;

  final RowPressedCallback onPressed;

  @override
  _ListTableState createState() => _ListTableState();
}

class _ListTableState extends State<ListTable> implements _TableDragUpdate {
  var columnWidths = Map<int, TableColumnWidth>();
  bool hasHiddenColumns;

  int hoveredIndex;
  int pressedIndex;
  int waitingIndex;

  Widget createHeader() {
    TableBorder tableBorder = widget.tableBorder;
    bool hasBorder = tableBorder != null && tableBorder.top != BorderSide.none;

    int lastNonZero = colSizes.lastIndexWhere((elem) => elem > 0.0);

    return Container(
      decoration: hasBorder
          ? BoxDecoration(border: Border(bottom: tableBorder.top))
          : null,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: List<Widget>.generate(colCount, (col) {
          assert(col < colSizes.length);

          if (colSizes[col] == 0.0) {
            return Container();
          }

          Widget result;

          if (colCount > 1 && col < colCount - 1) {
            result = Row(
              children: [
                Expanded(
                  child: LayoutBuilder(
                    builder: (context, constraints) {
                      return widget.tableHeaderBuilder(
                        context,
                        col,
                        constraints,
                      );
                    },
                  ),
                ),
                _TableColHandler(
                  tableDragUpdate: this,
                  col: col,
                  hasIndicator: (widget.showHiddenColumnsIndicator ?? false) &&
                      hasHiddenColumns &&
                      lastNonZero == col,
                  border:
                      widget.headerColumnBorder ?? tableBorder?.verticalInside,
                ),
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
            );
          } else {
            result = widget.tableHeaderBuilder(
              context,
              col,
              BoxConstraints.tightFor(
                width: colSizes[col],
                height: _kHeaderHeight,
              ),
            );
          }

          return ConstrainedBox(
            constraints: BoxConstraints.tightFor(
              height: _kHeaderHeight,
              width: colSizes[col],
            ),
            child: result,
          );
        }).toList(),
      ),
    );
  }

  Widget createList(int index) {
    int lastNonZero = colSizes.lastIndexWhere((elem) => elem > 0.0);

    return MouseRegion(
      onEnter: (_) => setState(() => hoveredIndex = index),
      onExit: (_) => setState(() => hoveredIndex = -1),
      child: GestureDetector(
        behavior: HitTestBehavior.deferToChild,
        onTapDown: (_) => setState(() => pressedIndex = index),
        onTapUp: (_) => setState(() => pressedIndex = -1),
        onTapCancel: () => setState(() => pressedIndex = -1),
        onTap: () {
          if (widget.onPressed != null) {
            if (waitingIndex == index) return;
            waitingIndex = index;
            dynamic result = widget.onPressed(index) as dynamic;

            if (result is Future) {
              setState(() => waitingIndex = index);
              result.then((_) => setState(() => waitingIndex = -1));
            } else {
              waitingIndex = -1;
            }
          }
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          children: List<Widget>.generate(colCount, (col) {
            assert(col < colSizes.length);

            Widget result = LayoutBuilder(
              builder: (context, constraints) => widget.tableRowBuilder(
                context,
                index,
                col,
                constraints,
              ),
            );

            result = Align(alignment: Alignment.bottomLeft, child: result);

            Color backgroundColor =
                pressedIndex == index || waitingIndex == index
                    ? Theme.of(context).colorScheme.overlay4
                    : hoveredIndex == index
                        ? Theme.of(context).colorScheme.overlay2
                        : null;

            BoxDecoration decoration = BoxDecoration(color: backgroundColor);

            if (widget.tableBorder != null &&
                    widget.tableBorder.horizontalInside != BorderSide.none ||
                widget.tableBorder.verticalInside != BorderSide.none) {
              final isBottom = index < widget.itemCount - 1 || hasExtent;
              final isRight = col < widget.colCount - 1 && col < lastNonZero;

              final horizontalInside = widget.tableBorder.horizontalInside;
              final verticalInside = widget.tableBorder.verticalInside;

              final border = Border(
                bottom: isBottom ? horizontalInside : BorderSide.none,
                right: isRight ? verticalInside : BorderSide.none,
              );

              decoration = decoration.copyWith(border: border);
            }

            return Container(
              constraints: BoxConstraints.tightFor(
                width: colSizes[col],
              ),
              decoration: decoration,
              child: result,
            );
          }),
        ),
      ),
    );
  }

  List<double> colSizes;
  Map<int, double> colFraction;

  double previousWidth;
  double totalWidth;
  List<double> previousColSizes;
  Map<int, double> previousColFraction;

  int get colCount => colSizes.length;

  ScrollController currentController;
  ScrollController get controller =>
      widget.controller ?? (currentController ??= ScrollController());

  @override
  void dragStart(int col) {
    previousColFraction = Map<int, double>.from(colFraction);
    previousColSizes = List<double>.from(colSizes);

    previousWidth = colSizes.sublist(col).reduce((v, e) => v + e);
    totalWidth = colSizes.reduce((v, e) => v + e);
  }

  @override
  void dragUpdate(int col, double delta) {
    setState(() {
      if (delta < 0) {
        delta = delta.clamp(-previousColSizes[col] + _kMinColumnWidth, 0.0);
      } else {
        delta = delta.clamp(0.0, previousWidth);
      }

      double newWidth = previousColSizes[col] + delta;
      colFraction[col] = newWidth / totalWidth;

      int totalRemain = colCount - (col + 1);

      if (totalRemain > 0) {
        double valueEach = delta / totalRemain;
        double remWidth = previousWidth - newWidth;

        for (int i = col + 1; i < colCount; i++) {
          if (remWidth >= _kMinColumnWidth) {
            double newWidth = (previousColSizes[i] - valueEach)
                .clamp(_kMinColumnWidth, remWidth);
            colFraction[i] = newWidth / totalWidth;
            remWidth -= newWidth;
          } else {
            colFraction[i] = 0.0;
          }
        }
      }
    });
  }

  @override
  void dragEnd() {
    totalWidth = null;
    previousWidth = null;
    previousColSizes = null;
    previousColFraction = null;
  }

  @override
  void dragCancel() => dragEnd();

  @override
  void initState() {
    super.initState();
  }

  bool hasExtent = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    WidgetsBinding.instance.addPostFrameCallback((Duration duration) {
      final position = controller.position;
      position.didUpdateScrollPositionBy(0.0);
      //hasExtent = position.maxScrollExtent > position.minScrollExtent;
    });
  }

  bool _handleScrollNotification(ScrollNotification notification) {
    final ScrollMetrics metrics = notification.metrics;

    if (notification.depth == 0) {
      final y = metrics.maxScrollExtent <= metrics.minScrollExtent;
      if (hasExtent != y) {
        setState(() => hasExtent = y);
      }
    }

    return false;
  }

  @override
  Widget build(BuildContext context) {
    Widget result = LayoutBuilder(
      builder: (context, constraints) {
        int colCount = widget.colCount;
        colFraction ??=
            Map<int, double>.from(widget.colFraction ?? Map<int, double>());
        colSizes = List<double>.filled(colCount, 0);

        double toltalWidth = constraints.maxWidth;
        double remWidth = toltalWidth;

        // TODO: make sure this is considering only the valid indexes
        int nfactors = 0;
        colFraction.keys.forEach((value) {
          if (value < colCount) nfactors += 1;
        });

        if (nfactors > 0) {
          for (int i = 0; i < colCount; i++) {
            if (remWidth <= 0.0) {
              remWidth = 0.0;
              break;
            }

            if (colFraction.containsKey(i)) {
              if (remWidth >= _kMinColumnWidth) {
                // the last item
                if (nfactors == colCount && i == colCount - 1) {
                  colSizes[i] = remWidth;
                  remWidth = 0.0;
                  break;
                }

                double width = (colFraction[i] * toltalWidth)
                    .roundToDouble()
                    .clamp(_kMinColumnWidth, remWidth);
                colSizes[i] = width;
                remWidth -= width;

                assert(remWidth >= 0.0,
                    'Wrong fraction value at $i value ${colFraction[i]}.');
              }
            }
          }
        }

        // if there's no key for every index in columns
        if (nfactors < colCount) {
          int remNfactors = colCount - nfactors;
          double nonFactorWidth = (remWidth / remNfactors).roundToDouble();

          assert(remWidth >= 0.0);

          for (int i = 0; i < colCount; i++) {
            if (!colFraction.containsKey(i)) {
              remNfactors -= 1;

              if (remWidth < _kMinColumnWidth) {
                colFraction[i] = 0.0;
                continue;
              }

              // last item
              if (i == colCount - 1 || remNfactors == 0) {
                colSizes[i] = remWidth;
                colFraction[i] = remWidth / toltalWidth;
                remWidth = 0;
                break;
              }

              if (nonFactorWidth > remWidth) {
                nonFactorWidth = remWidth;
              }

              colFraction[i] = nonFactorWidth / toltalWidth;

              colSizes[i] = nonFactorWidth;
              remWidth -= nonFactorWidth;
            }
          }
        }

        if (remWidth > 0.0) {
          colSizes[colSizes.lastIndexWhere((value) => value > 0.0)] += remWidth;
          remWidth = 0.0;
        }

        hasHiddenColumns = !colSizes.every((elem) => elem > 0.0);

        return Column(
          children: [
            createHeader(),
            Expanded(
              child: ListView.custom(
                childrenDelegate: SliverChildBuilderDelegate(
                  (context, index) => createList(index),
                  childCount: widget.itemCount,
                ),
                controller: controller,
                itemExtent: widget.itemExtent,
              ),
            ),
          ],
        );
      },
    );

    final tableBorder = widget.tableBorder;

    if (tableBorder != null &&
        (tableBorder.left != BorderSide.none ||
            tableBorder.right != BorderSide.none ||
            tableBorder.top != BorderSide.none ||
            tableBorder.bottom != BorderSide.none)) {
      result = Container(
        decoration: BoxDecoration(
          border: Border(
            left: tableBorder.left,
            right: tableBorder.right,
            top: tableBorder.top,
            bottom: tableBorder.bottom,
          ),
        ),
        child: result,
      );
    }

    return NotificationListener<ScrollNotification>(
      onNotification: _handleScrollNotification,
      child: result,
    );
  }
}

abstract class _TableDragUpdate {
  void dragStart(int col);
  void dragUpdate(int col, double value);
  void dragEnd();
  void dragCancel();
}

class _TableColHandler extends StatefulWidget {
  _TableColHandler({
    @required this.tableDragUpdate,
    @required this.col,
    @required this.hasIndicator,
    this.border,
    Key key,
  })  : assert(tableDragUpdate != null),
        assert(col != null),
        assert(hasIndicator != null),
        super(key: key);

  final bool hasIndicator;
  final _TableDragUpdate tableDragUpdate;
  final int col;
  final BorderSide border;

  @override
  _TableColHandlerState createState() => _TableColHandlerState();
}

class _TableColHandlerState extends State<_TableColHandler>
    with ComponentStateMixin {
  Map<Type, GestureRecognizerFactory> get _gestures {
    final gestures = <Type, GestureRecognizerFactory>{};

    gestures[HorizontalDragGestureRecognizer] =
        GestureRecognizerFactoryWithHandlers<HorizontalDragGestureRecognizer>(
      () => HorizontalDragGestureRecognizer(
        debugOwner: this,
      ),
      (HorizontalDragGestureRecognizer instance) {
        instance
          ..dragStartBehavior = DragStartBehavior.down
          ..onStart = _handleDragStart
          ..onDown = _handleDragDown
          ..onUpdate = _handleDragUpdate
          ..onCancel = _handleDragCancel
          ..onEnd = _handleDragEnd;
      },
    );

    return gestures;
  }

  double position;
  double minOffset;
  double maxOffset;
  double currentPosition;
  _TableDragUpdate get tableUpdateColFactor => widget.tableDragUpdate;
  int get col => widget.col;

  void _handleDragStart(DragStartDetails details) {
    tableUpdateColFactor.dragStart(col);
    currentPosition = details.globalPosition.dx;
  }

  void _handleDragDown(DragDownDetails details) =>
      setState(() => dragged = true);

  void _handleDragUpdate(DragUpdateDetails details) {
    tableUpdateColFactor.dragUpdate(
        col, details.globalPosition.dx - currentPosition);
  }

  void _handleDragEnd(DragEndDetails details) {
    setState(() => dragged = false);
    tableUpdateColFactor.dragEnd();
  }

  void _handleDragCancel() {
    setState(() => dragged = false);
    tableUpdateColFactor.dragCancel();
  }

  void _handleMouseEnter(PointerEnterEvent event) =>
      setState(() => hovered = true);

  void _handleMouseExit(PointerExitEvent event) =>
      setState(() => hovered = false);

  @override
  Widget build(BuildContext context) {
    final hoveredColor = Theme.of(context).colorScheme.overlay10;
    final draggedColor = Theme.of(context).colorScheme.primary;

    BorderSide border = widget.border;
    bool hasFocus = hovered || dragged || widget.hasIndicator;

    if (border != null && border != BorderSide.none) {
      final borderColor =
          dragged ? draggedColor : hovered ? hoveredColor : border.color;

      border = border.copyWith(
          color: borderColor,
          width: hasFocus
              ? border.width + (border.width / 2.0).roundToDouble()
              : border.width);
    } else {
      final color = Theme.of(context).colorScheme.overlay6;
      final width = hasFocus ? 2.0 : 1.0;
      final borderColor =
          dragged ? draggedColor : hovered ? hoveredColor : color;
      border = BorderSide(width: width, color: borderColor);
    }

    return RawGestureDetector(
      gestures: _gestures,
      behavior: HitTestBehavior.translucent,
      child: MouseRegion(
        opaque: false,
        cursor: SystemMouseCursors.click,
        onEnter: _handleMouseEnter,
        onExit: _handleMouseExit,
        child: Container(
          margin: EdgeInsets.only(
            left: (_kHandlerWidth - border.width).clamp(0.0, double.infinity),
          ),
          decoration: BoxDecoration(border: Border(right: border)),
        ),
      ),
    );
  }
}
